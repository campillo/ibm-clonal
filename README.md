# IBM clonal

More details in this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/ibm-clonal/index.html).

## Description
An Individual-based model simulator for clonal plant growth.
See this [paper](https://www.sciencedirect.com/science/article/abs/pii/S030438001200124X).

## Visuals
See this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/smc-demos/index.html).


## Usage

Just run `main_clonal.m` under `matlab`.



## Authors and acknowledgment

I use `circ_vmpdf.m` and  `circ_vmrnd.m` from
[Circular Statistics Toolbox (Directional Statistics)](https://it.mathworks.com/matlabcentral/fileexchange/10676-circular-statistics-toolbox-directional-statistics) of [Philipp Berens](https://www.eye-tuebingen.de/berens/) 


## License
[License](License.md)


## Project status
IBM clonal is written with an ''old'' matlab, an update in a recent matlab would be welcome. 



## AUTHOR

IBM clonal is conceived by 
[Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html), [MathNeuro Team](https://team.inria.fr/mathneuro/)
and [Nicolas Champagnat](http://nchampagnat.perso.math.cnrs.fr);
it is written by
[Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html), [MathNeuro Team](https://team.inria.fr/mathneuro/), [Inria centre
at Université Côte d’Azur](https://www.inria.fr/en/inria-centre-universite-cote-azur)
Fabien.Campillo@inria.fr
